var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_listJson = function (req, res) {
    Bicicleta.allBicis(function (err, bicis) {
        res.status(200).json({
            Bicicletas: bicis
        });
    });
}


exports.bicicleta_list = function (req, res) {
    Bicicleta.allBicis(function (err, bicis) {
        res.status(200).render('bicicletas/index', {
            bicis: bicis
        });
    });

}

exports.bicicleta_create = function (req, res, next) {
    var bici = new Bicicleta({
        code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng]
    });

    bici.save((err, result) => {
        if (err) console.log(err);
        // console.log(res);
    });
    
    res.status(200);
    // next();
    // res.redirect('/bicicletas');    
}

// Editar bicicleta : GET
exports.bicicleta_update_get = function (req, res) {
    // const code = req.params.code;
    // console.log(code);

    const id = req.params._id;


    Bicicleta.findIt(id, function (err, bici) {
        if (err) console.log(err);
        res.render('bicicletas/update', {
            bici
        });
    });

}

// Editar bicicleta : POST
exports.bicicleta_update_post = async function (req, res) {

    const id = req.params._id
    var bici = await Bicicleta.findIt(id);

    bici.code = req.body.code;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];

    bici.save();

    res.redirect('/bicicletas');
}


exports.bicicleta_delete = function (req, res, next) {
    const id = req.params._id;
    console.log(id);
    Bicicleta.removeIt(id, function (err, res) {
        if (err) console.log(err);
        console.log(res);
    });

    //Set HTTP method to GET
    // req.method = 'DELETE';


    //redirect
    res.redirect('/bicicletas');

}