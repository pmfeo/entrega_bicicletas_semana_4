const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;

if (process.env.NODE_ENV === 'production') {
    console.info('*******************************production environment*******************************');
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    console.log(options);
    mailConfig = sgTransport(options);
    console.log('mailConfig passed');
} else {
    if (process.env.NODE_ENV === 'staging') {
        console.info('*******************************staging environment*******************************');
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_SECRET
            }
        }
        mailConfig = sgTransport(options);
    } else {
        mailConfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: process.env.ethereal_user,
                pass: process.env.ethereal_pwd
            }
        }
    }
}

// const mailConfig = {
//     host: 'smtp.ethereal.email',
//     port: 587,
//     auth: {
//         user: 'rachelle.rippin@ethereal.email',
//         pass: 'ZnQaDnKfwSZQxjzEUf'
//     }
// }


module.exports = nodemailer.createTransport(mailConfig);