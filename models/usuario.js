var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Reserva = require('./reserva');

const bcrypt = require('bcrypt');
const crypto = require('crypto');
const uniqueValidator = require('mongoose-unique-validator');

const saltRounds = 10;


const Token = require('../models/token');
const mailer = require('../mailer/mailer');


const production = 'https://red-bicicletas-v4.herokuapp.com';
const development = 'http://localhost:3000';
const homeUrl = (process.env.NODE_ENV ? production : development);
// console.log(`Home URL is: ${homeUrl}`);

const validateEmail = function (email) {
    const reg = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    return reg.test(email);
}

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor ingrese un email válido'],
        match: [/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/]
    },
    password: {
        type: String,
        // trim: true,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

usuarioSchema.plugin(uniqueValidator, {
    message: 'El {PATH} ya existe para este usuario'
});

usuarioSchema.pre('save', function (next) {
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.resetPassword = function (cb) {
    const token = new Token({
        _userId: this._id,
        token: crypto.randomBytes(16).toString('hex')
    });

    const email_destination = this.email;

    token.save(function (err) {
        if (err) {
            return cb(err);
        }

        const mailOptions = {
            from: 'pablo.feo.acosta@gmail.com',
            to: email_destination,
            subject: 'Reseteo de password',
            text: 'Hola, \n\n' + 'Por favor, para verificar su cuenta haga click en el link debajo o cópialo y pégalo en tu navegador: \n' + `${homeUrl}/resetPassword/${token.token}` + ' .\n'
            // 'http://localhost:3000' + '\/resetPassword\/' + token.token + '.\n'
        };

        mailer.sendMail(mailOptions, function (err) {
            if (err) {
                return cb(err);
            }
            console.log('Se ha enviado un email para resetear el password a ' + email_destination + '.');
        });

        cb(null);

    });
}

usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {
    var reserva = new Reserva({
        usuario: this._id,
        bicicleta: biciId,
        desde: desde,
        hasta: hasta,
    });
    // console.log(reserva);
    reserva.save(cb);
}

usuarioSchema.methods.enviar_email_bienvenida = function (cb) {
    const token = new Token({
        _userId: this._id,
        token: crypto.randomBytes(16).toString('hex')
    });
    const email_destination = this.email;

    token.save(function (err) {
        if (err) console.log(err);

        const mailOptions = {
            from: 'pablo.feo.acosta@gmail.com',
            to: email_destination,
            subject: 'Verificacion de cuenta',
            text: 'Hola, \n\n' + 'Por favor, para verificar su cuenta haga click en el link debajo o cópialo y pégalo en tu navegador: \n' + `${homeUrl}/token/confirmation/${token.token}` + ' .\n'
            // 'http://localhost:3000' + '\/token/confirmation\/' + token.token + '.\n'
        };

        mailer.sendMail(mailOptions, function (err) {
            if (err) {
                return console.log(err.message);
            }
            console.log('Se ha enviado un email de bienvenida a ' + email_destination + '.');
        });

    });
}

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log('---CONDITION 1---');
    console.log(condition);
    self.findOne({
        '$or': [{
                'googleId': condition.id
            },
            {
                'email': condition.emails[0].value
            }
        ]
    }, (err, result) => {
        if (result) {
            callback(err, result);
        } else {
            console.log('---CONDITION 2---');
            console.log(condition);
            let values = {};
            values.googleId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'sin nombre';
            values.verificado = true;
            values.password = condition._json.etag;
            console.log('---VALUES---');
            console.log(values);
            self.create(values, (err, result) => {
                if (err) {
                    console.log(err);
                }
                return callback(err, result);
            })
        }
    })
}

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback) {
    const self = this;
    // console.log('---CONDITION 1---');
    // console.log(condition);
    console.log('starting findOneOrCreateByFacebook ');
    self.findOne({
        '$or': [{
                'facebookId': condition.id
            },
            {
                'email': condition.emails[0].value
            }
        ]
    }, (err, result) => {
        if (result) {
            callback(err, result);
        } else {
            console.log('---CONDITION 2---');
            console.log(condition);
            let values = {};
            values.facebookId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'sin nombre';
            values.verificado = true;
            values.password = crypto.randomBytes(16).toString('hex');
            console.log('---VALUES---');
            console.log(values);
            self.create(values, (err, result) => {
                if (err) {
                    console.log(err);
                }
                return callback(err, result);
            })
        }
    })
}

module.exports = mongoose.model('Usuario', usuarioSchema);