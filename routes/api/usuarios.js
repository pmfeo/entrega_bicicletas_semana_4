var express = require('express');
var router = express.Router();
var usuariosController = require('../../controllers/api/usuariosControllerAPI');


// LIST
router.get('/', usuariosController.usuarios_list);

// CREATE
router.post('/create', usuariosController.usuarios_create);

// RESERVAR
router.post('/reservar', usuariosController.usuarios_reservar);

module.exports = router;