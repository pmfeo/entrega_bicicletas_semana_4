const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
var GoogleStrategy = require('passport-google-oauth20').Strategy;
var FacebookTokenStrategy = require('passport-facebook-token');

const Usuario = require('../models/usuario');

const production = 'https://red-bicicletas-v4.herokuapp.com';
const development = 'http://localhost:3000';
const homeUrl = (process.env.NODE_ENV ? production : development);

passport.use(new LocalStrategy(
    function (email, password, done) {
        Usuario.findOne({
            email: email
        }, function (err, usuario) {
            if (err) {
                return done(err);
            }
            if (!usuario) {
                return done(null, false, {
                    message: 'Email no existe o Usuario incorrecto'
                });
            }
            if (!usuario.validPassword(password)) {
                return done(null, false, {
                    message: 'Password incorrecto'
                });
            }
            return done(null, usuario);
        })
    }
));


passport.use(new GoogleStrategy({
        clientID: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        callbackURL: `${homeUrl}/auth/google/callback`,
        passReqToCallback: true
    },
    function (accessToken, refreshToken, profile, cb) {
        console.log('--------PROFILE----------');
        console.log(profile);
        Usuario.findOneOrCreateByGoogle({
            googleId: profile.id
        }, function (err, user) {
            return cb(err, user);
        });
    }
));

passport.use(new FacebookTokenStrategy({
        clientID: process.env.FACEBOOK_ID,
        clientSecret: process.env.FACEBOOK_SECRET,
        // callbackURL: `${homeUrl}/api/auth/facebook_token`,
        // passReqToCallback: true
    },
    function (accessToken, refreshToken, profile, done) {
        console.log('starting facebook strategy, loggin profile...');
        console.log(profile);
        try {
            Usuario.findOneOrCreateByFacebook(profile, function (err, user) {
                if (err) {
                    console.log('err' + err);
                }
                return done(err, user);
            });
        } catch (err2) {
            console.log(err2);
            return done(err2, null);
        }
    }
));

passport.serializeUser(function (user, cb) {
    cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
    Usuario.findById(id, function (err, usuario) {
        cb(err, usuario);
    });
});

module.exports = passport;